/*
 * Copyright 2014 akshay.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.helloworldopen.bumblebee.model;

/**
 *
 * @author Akshay Dewan <akudewan@gmail.com>
 */
public class CarDimensions {

    private float length;
    private float width;
    private float guideFlagPosition;

    public float getLength() {
        return length;
    }

    public float getWidth() {
        return width;
    }

    public float getGuideFlagPosition() {
        return guideFlagPosition;
    }

    @Override
    public String toString() {
        return "CarDimensions{" + "length=" + length + ", width=" + width + ", guideFlagPosition=" + guideFlagPosition + '}';
    }

}
