/*
 * Copyright 2014 akshay.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.helloworldopen.bumblebee.model;

import java.util.Arrays;

/**
 *
 * @author Akshay Dewan <akudewan@gmail.com>
 */
public class Race {

    private Track track;
    private Lane[] lanes;
    private Car[] cars;
    private RaceSession raceSession;

    public Track getTrack() {
        return track;
    }

    public Lane[] getLanes() {
        return lanes;
    }

    public Car[] getCars() {
        return cars;
    }


    public RaceSession getRaceSession() {
        return raceSession;
    }

    @Override
    public String toString() {
        return "Race{" + "track=" + track + ", lanes=" + Arrays.toString(lanes) + ", cars=" + Arrays.toString(cars) + ", raceSession=" + raceSession + '}';
    }

}
