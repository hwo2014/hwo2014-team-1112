/*
 * Copyright 2014 Karun AB <me@karunab.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.helloworldopen.bumblebee.model;

/**
 *
 * @author Karun AB <me@karunab.com>
 */
public class CarPiecePosition {

    private final int pieceIndex;
    private final double inPieceDistance;
    private final CarPieceLane lane;
    private final int lap;

    public CarPiecePosition(int pieceIndex, double inPieceDistance, CarPieceLane lane, int lap) {
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
        this.lane = lane;
        this.lap = lap;
    }

    public int getPieceIndex() {
        return pieceIndex;
    }

    public double getInPieceDistance() {
        return inPieceDistance;
    }

    public CarPieceLane getLane() {
        return lane;
    }

    public int getLap() {
        return lap;
    }

    @Override
    public String toString() {
        return "CarPiecePosition {" + "\npieceIndex=" + pieceIndex + ",\ninPieceDistance=" + inPieceDistance + ",\nlane=" + lane + ",\nlap=" + lap + "}\n";
    }

}
