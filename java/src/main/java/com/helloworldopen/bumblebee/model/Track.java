/*
 * Copyright 2014 Akshay Dewan <akudewan@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.helloworldopen.bumblebee.model;

import java.util.Arrays;

/**
 *
 * @author Akshay Dewan <akudewan@gmail.com>
 */
public class Track {

    private String id;
    private String name;
    private TrackPiece[] pieces;
    private Lane[] lanes;
    private StartingPoint startingPoint;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public TrackPiece[] getPieces() {
        return pieces;
    }
    
    public Lane[] getLanes() {
        return lanes;
    }

    public StartingPoint getStartingPoint() {
        return startingPoint;
    }

    @Override
    public String toString() {
        return "Track{" + "id=" + id + ", name=" + name + ", pieces=" + Arrays.toString(pieces) + ", lanes=" + Arrays.toString(lanes) + ", startingPoint=" + startingPoint + '}';
    }

}
