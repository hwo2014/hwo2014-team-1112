/*
 * Copyright 2014 Karun AB <me@karunab.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.helloworldopen.bumblebee.message.events;

import com.helloworldopen.bumblebee.model.SendMsg;

/**
 *
 * @author Karun AB <me@karunab.com>
 */
public class SwitchLane extends SendMsg {

    private final String data;

    public static SwitchLane get(LaneDirection laneDirection) {
        return new SwitchLane(laneDirection.toString());
    }

    private SwitchLane(final String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    @Override
    protected Object msgData() {
        return data;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }

    public enum LaneDirection {

        LEFT("Left"), RIGHT("Right");

        private String directionString;

        LaneDirection(final String directionStr) {
            this.directionString = directionStr;
        }

        @Override
        public String toString() {
            return directionString;
        }
    }
}
