/*
 * Copyright 2014 akshay.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.helloworldopen.bumblebee.message.events;

import com.helloworldopen.bumblebee.model.SendMsg;

/**
 *
 * @author Akshay Dewan <akudewan@gmail.com>
 */
public class CreateRace extends SendMsg {

    private Bot botId;
    private String trackName;
    private String password;
    private int carCount = 1;

    public CreateRace trackName(String trackName) {
        this.trackName = trackName;
        return this;
    }

    public CreateRace password(String password) {
        this.password = password;
        return this;
    }

    public CreateRace carCount(int carCount) {
        this.carCount = carCount;
        return this;
    }

    public CreateRace botName(String botName) {
        if (this.botId == null) {
            this.botId = new Bot();
        }
        this.botId.setName(botName);
        return this;
    }

    public CreateRace botKey(String botKey) {
        if (this.botId == null) {
            this.botId = new Bot();
        }
        this.botId.setKey(botKey);
        return this;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }

}
