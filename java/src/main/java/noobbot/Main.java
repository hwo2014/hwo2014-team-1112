/*
 * Copyright 2014 Karun AB <me@karunab.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;
import com.helloworldopen.bumblebee.message.events.Join;
import com.helloworldopen.bumblebee.message.events.JoinRace;
import com.helloworldopen.bumblebee.model.MsgWrapper;
import com.helloworldopen.bumblebee.message.events.Ping;
import com.helloworldopen.bumblebee.message.events.SwitchLane;
import com.helloworldopen.bumblebee.model.SendMsg;
import com.helloworldopen.bumblebee.message.events.Throttle;
import com.helloworldopen.bumblebee.model.CarPosition;
import com.helloworldopen.bumblebee.model.GameInit;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Karun AB <me@karunab.com>
 */
public class Main {

    private PrintWriter writer;

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, botName, new Join(botName, botKey));
        
        //TODO additional parameters can be parsed from the command line args
        //new Main(reader, writer, botName, new Join(botName, botKey));
        //new Main(reader, writer, botName, new JoinRace().botName(botName).botKey(botKey).trackName("germany").password(randomPasswd));
    }

    public Main(final BufferedReader reader, final PrintWriter writer, final String botName, final SendMsg join) throws IOException {
        final Gson gson = new Gson();
        final List<Integer> rightSwitchIndexList = Arrays.asList(2);
        final List<Integer> leftSwitchIndexList = Arrays.asList(2);
        GameInit gameInit = null;

        this.writer = writer;
        String line;

        send(join);

        while ((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            switch (msgFromServer.getMsgType()) {
                case "carPositions":
//                    final CarPosition[] carPosArray = gson.fromJson(msgFromServer.getData().toString(), CarPosition[].class);
//                    for (final CarPosition carPos : carPosArray) {
//                        if (botName.equals(carPos.getId().getName())) {
//                            if (rightSwitchIndexList.contains(carPos.getPiecePosition().getPieceIndex())) {
//                                send(SwitchLane.get(SwitchLane.LaneDirection.RIGHT));
//                            } else if (leftSwitchIndexList.contains(carPos.getPiecePosition().getPieceIndex())) {
//                                send(SwitchLane.get(SwitchLane.LaneDirection.LEFT));
//                            }
//                        }
//                    }
                    send(new Throttle(0.625));
                    //Git Push Test Comment
                    break;
                case "join":
                    System.out.println("Joined: " + msgFromServer.getData());
                    break;
                case "gameInit":
                    gameInit = gson.fromJson(msgFromServer.getData().toString(), GameInit.class);
                    System.out.println("Race init");
                    System.out.println("Init data");
                    System.out.println(gameInit.toString());
                    break;
                case "gameEnd":
                    System.out.println("Race end");
                    break;
                case "gameStart":
                    System.out.println("Race start");
                    break;
                default:
                    System.out.println("Unknown: " + msgFromServer.getData());
                    send(new Ping());
                    break;
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}
